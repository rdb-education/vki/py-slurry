""" This module contains the classes for all the kind of fluids (like a pure
fluid, a slurry...)
(possibly in future accounting for thermodynamics) """


class Fluid():

    """ Class that represents an incompressible fluid.

    Attributes:
        mu (float):  viscosity of the fluid                 [Pa*s]
        rho (float): density of the fluid                   [kg*m^3]

    Examples:

        Float call:

        >>> water = Fluid(0.89E-3, 1000)
        >>> nitrogen = Fluid(0.292E-3*, 867.86)

    """

    def __init__(self, mu, rho, *args, **kwargs):
        """ Constructor

        Args:
            mu (float):  viscosity of the fluid             [Pa*s]
            rho (float): density of the fluid               [kg/m^3]

        """

        self.mu = mu
        self.rho = rho


class Slurry(Fluid):
    """ Class that represents an incompressible monospecie slurry.

    Attributes:
        mu(float):    viscosity of the liquid carrier        [Pa*s]
        rho(float):   density of the liquid carrier          [kg/m^3]
        rho_s(float): density of the solid particles         [kg/m^3]
        d(float):     diameter of the solid particles        [m]
        C(float):     mean volume concentration of the solid []
        s (float):     relative density (rho_s/rho)          []

    Examples:

        **Durand and Condolios (1952) Sand Slurries [L8]**

        "Communication de R. Durand et E. Condolios", *Compte Rendu des
        Deuxiemes Journees de L'Hydraulique*, 89-103 (1953)

        >>> sand_slurry = Slurry(0.89E-3, 1000, 1000*2.65, 2.04)

    """

    def __init__(self, mu, rho, rho_s, d, C, *args, **kwargs):
        """ Constructor

        Args:
            mu(float):     viscosity of the liquid carrier        [Pa*s]
            rho(float):    density of the liquid carrier          [kg/m^3]
            rho_s(float):  density of the solid particles         [kg/m^3]
            d(float):      diameter of the solid particles        [m]
            C(float):      mean volume concentration of the solid []
        """

        super().__init__(mu, rho, *args, **kwargs)
        self.rho_s = rho_s
        self.d = d
        self.s = self.rho_s/self.rho
        self.C = C


class Water(Fluid):
    """ Useful class that represents incompressible water at 25C """

    def __init__(self, *args, **kwargs):
        mu = 1.006E-3
        rho = 1000
        super().__init__(mu, rho, *args, **kwargs)


class SlurryWater(Water, Slurry):
    """ Useful class that represents water slurry """

    def __init__(self, rho_s, d, C, *args, **kwargs):
        """ Constructor

        Args:
            rho_s(float):  density of the solid particles         [kg/m^3]
            d(float):      diameter of the solid particles        [m]
            C(float):      mean volume concentration of the solid []

        """

        super().__init__(rho_s, d, C, *args, **kwargs)


class Nitrogen(Fluid):
    """ Useful class that represents incompressible nitrogen """

    def __init__(self, *args, **kwargs):
        mu = 0.292E-3
        rho = 867.86
        super().__init__(mu, rho, *args, **kwargs)


class SlushNitrogen(Nitrogen, Slurry):
    """ Useful class that represents slush nitrogen """

    def __init__(self, d, C, *args, **kwargs):
        rho_s = 1026

        super().__init__(d=d, C=C, rho_s=rho_s)
