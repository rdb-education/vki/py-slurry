from setuptools import setup, find_packages

# from setuptools.command.build_ext import build_ext as _build_ext

"""class build_ext(_build_ext):
    'to install numpy'
    def finalize_options(self):
        _build_ext.finalize_options(self)
        # Prevent numpy from thinking it is still in
        # its setup process:
        __builtins__.__NUMPY_SETUP__ = False
        import numpy
        self.include_dirs.append(numpy.get_include())
"""
setup(
    name="pyslurry",
    version="0.1",
    packages=find_packages(),

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=[
        'scipy',
    ],

    extras_require={
        'docs': ['sphinx'],
        'examples': ['jupyter'],
    },

    package_data={
        '': ['README.rst'],
    },

    # metadata for upload to PyPI
    author="Ruben Di Battista",
    author_email="ruben.dibattista@vki.ac.be",
    description="A library to perform estimation about general properties"
                "of slurry flows inside typical geometries (i.e. pipes)",
    license="GPLv3",
    keywords="fluid dynamics pipe slurry multiphase flow",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
    ]



    # could also include long_description, download_url, classifiers, etc.
)
